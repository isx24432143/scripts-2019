#! /bin/bash
# Jamison David Quelal León ASIX-M01-ISO
# Febrer 2020
# Exemples for
#--------------------------------------

#Mostrar i numerar llista arguments
nlin=0
for arg in $*
do
  nlin=$((nlin+1))
  echo "$nlin: $arg"
done
exit 0

#Iterar per la llista d'arguments
for arg in "$@"
do
  echo $arg
done
exit 0

#Iterar per la llista d'argument
for arg in $*
do
  echo $arg
done
exit 0

#Iterar per un contingut d'una variable
llistat=$(ls)
for nom in $llistat
do
  echo $nom
done
exit 0

#Iterar per el contingut d'una cadena
#Només iterar un cop, una sola cadena
for nom in "pere pau marta anna"
do
  echo $nom
done
exit 0

#Iterar per un conjunt d'elements
for nom in "pere" "pau" "marta" "anna"
do
	  echo $nom
  done

exit 0
