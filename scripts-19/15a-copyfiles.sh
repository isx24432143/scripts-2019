#! /bin/bash
# Jamison David Quelal León ASIX-M01-ISO
# Febrer 2020
# Donat un arg: file i 2arg: dir, copia el file al directori
#--------------------------------------
ERR_NARGS=1
ERR_NODIR=2
ERR_NOREGFILE=3
OK=0

#Validar Argument
if [ $# -ne 2 ]; then
  echo "ERROR: num args incorrecte"
  echo "USAGE: $0 file dir-destí"
  exit $ERR_NARGS
fi
#Validar si $2 és un directori
if ! [ -d $2 ]; then
  echo "ERROR: $2 no és un directori"
  echo "USAGE: $0 file dir-destí"
  exit 2
fi
#3) Verificar regular file
if [ ! -f $1 ]; then
  echo "ERROR: $1 no és un regular file"
  echo "USAGE: $0 file dir-destí"
  exit $ERR_NOREGFILE
fi

#Programa
$file=$1
$dir=$2
cp $file $dir
