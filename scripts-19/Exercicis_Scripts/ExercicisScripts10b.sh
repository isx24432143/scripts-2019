#! /bin/bash
FI=0
while read -r gid
do
  liniaGid=$(grep "^[^:]*:[^:]*:$gid:" /etc/group)
  if [ $? -eq 0 ]; then
   gname=$(echo $liniaGid | cut -d: -f1 | tr '[a-z]' '[A-Z]')
   userList=$(echo $liniaGid | cut -d: -f4 | tr '[a-z]' '[A-Z]')
   echo "gname: $gname gid: $gid users: $userList"
  else
   echo "ERROR: $gid inexistent" >> /dev/stderr
  fi
done
exit $FI:
