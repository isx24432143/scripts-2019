# /bin/bash
# Jamison David Quelal León ASIX-M01-ISO
# Febrer 2020
# 4) Processar stdin cmostrant per stdout les línies numerades i en majúscules..
#-------------------------------------------
FI=0
#Programa
numlinia=1
while read -r line
do
  echo "$numlinia: $line" | tr '[a-z]' '[A-Z]'
  numlinia=$((numlinia+1))
done
exit $FI
