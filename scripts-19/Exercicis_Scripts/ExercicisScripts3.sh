# /bin/bash
# Jamison David Quelal León ASIX-M01-ISO
# Febrer 2020
# 3) Processar arguments que són matricules:
#  a) llistar les vàlides, del tipus: 9999-AAA.
#  b) stdout les que són vàlides, per stderr les no vàlides. Retorna de status el número d̉errors (de no vàlides)
#------------------------------------------------
ERR_ARGS=1
FI=0
#Validar arguments
if [ $# -eq 0 ]; then
  echo "ERROR: nº args invàlid"
  echo "USAGE: $0 matricules[9999-AAA][...]"
  exit $ERR_ARGS
fi
#:Programa
errors=0
for arg in $*
do
  echo $arg | egrep "^[[:digit:]]{4}-[[:upper:]]{3}$" &> /dev/null
  if [ $? -ne 0 ]; then
    echo "$arg" >> /dev/stderr
    errors=$((errors+1))
  else
    echo $arg
  fi
done
echo $errors
exit $FI
