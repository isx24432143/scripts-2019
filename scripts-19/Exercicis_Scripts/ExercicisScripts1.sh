# /bin/bash
# Jamison David Quelal León ASIX-M01-ISO
# Febrer 2020
# 1) Processar els arguments i mostrar per stdout només els de 4 o més caràcters.
#-----------------------------------------------
ERR_ARGS=1
FI=0
#validar Arguments
if [ $# -eq 0 ]; then
  echo "ERROR: nº args no vàlid"
  echo "USAGE: $0 args[...]"
  exit $ERR_ARGS
fi
#Programa
for arg in $*
do
  num=$(echo "$arg" | wc -L)
  if [ $num -ge 4 ]; then
    echo $arg
  fi
done
exit $FI
