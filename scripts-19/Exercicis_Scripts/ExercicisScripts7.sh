# /bin/bash
# Jamison David Quelal León ASIX-M01-ISO
# Febrer 2020
# 7) Programa: prog -f|-d arg1 arg2 arg3 arg4
# a) Valida que els quatre arguments rebuts són tots del tipus que indica el flag. 
# És a dir, si es crida amb -f valida que tots quatre són file. Si es crida amb -d valida que tots quatre són directoris.
#     Retorna 0 ok, 1 error nº args, 2 hi ha elements errònis.
#         Exemple: prog -f carta.txt a.txt /tmp fi.txt → retorna status 2.
#	  b) Ampliar amb el cas: prog -h|--help.
#----------------------------------------------------
OK=0
ERR_ARGS=1
#Validem arg
if [ $# -ne 5 ]; then
  echo "ERROR: Nº arg erroni"
  echo "USAGE: $0 -f|-d arg1 arg2 arg3 arg4"
  exit $ERR_ARGS
fi
#Programa
status=0
if [ "$1" != "-f" -a "$1" != "-d"  ]
then
  echo "ERROR format arguments"
  echo "usage: 07_valida_arg.sh -f|-d arg1 arg2 arg3 arg4"
  exit $ERR_ARGS
fi
tipus=$1
shift
for arg in $*
do
    if ! [ $tipus "$arg" ]; then
    status=2
  fi
done
exit $status
