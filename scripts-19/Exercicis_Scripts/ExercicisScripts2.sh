# /bin/bash
# Jamison David Quelal León ASIX-M01-ISO
# Febrer 2020
# 2) Processar els arguments i comptar quantes n̉hi ha de 3 o més caràcters.
#-------------------------------------------
ERR_ARGS=1
FI=0
#validar Arguments
if [ $# -eq 0 ]; then
  echo "ERROR: nº args no vàlid"
  echo "USAGE: $0 args[...]"
  exit $ERR_ARGS
fi
#Programa
compt=0
for arg in $*
do
  num=$(echo "$arg" | wc -L)
  if [ $num -ge 3 ]; then
   compt=$((compt+1))
  fi
done
echo $compt
exit $FI
