#! /bin/bash
# Jamison David Quelal León
# Curs 2019-2020
# Març 2020
#---------------------------
#showAllGroupMainMembers 
function showAllGroupMainMembers(){
   llistaGname=$(cut -d: -f1 /etc/group)
   for gname in $llistaGname
   do
     echo "$gname:"
     grep "^$gname:" /etc/group | cut -d: -f4 | sort | sed -r 's/^(.*)$/\t\1/g'
   done
}

#showShadow login
function showShadow(){
   ERR_NARGS=1
   NOLOGIN=2
   if [ $# -ne 1 ]; then
     echo "ERROR: num args incorrecte"
     echo "USAGE: $0 login"
     return $ERR_NARGS
    fi
    #Validem si el login existeix
    login=$1
    linia=$(grep "^$login:" /etc/shadow 2> /dev/null)
    if [ $? -ne 0 ]; then
     echo "ERROR: $login login no existeix"
     echo "USAGE: $0 login"
     return $NOLOGIN
    else
     passwd=$(echo "$linia" | cut -d: -f2)
     lastpasswdchg=$(echo "$linia" | cut -d: -f3)
     datelast=$(echo "$linia" | cut -d: -f4)
     minpasswd=$(echo "$linia" | cut -d: -f5)
     maxpasswd=$(echo "$linia" | cut -d: -f6)
     passwdexpi=$(echo "$linia" | cut -d: -f7)
     passwdinact=$(echo "$linia" | cut -d: -f8)
     accountexpidate=$(echo "$linia" | cut -d: -f9)
     echo "Login: $login"
     echo "Password: $passwd"
     echo "Date of Last passwd change: $datelast"
     echo "Minimum passwd age: $minpasswd"
     echo "Maximum passwd age: $maxpasswd"
     echo "Passwd warning period: $passwdexpi"
     echo "Passwd inactivity period: $passwdinact"
     echo "Account expiration date: $accountexpidate"
     return 0    
    fi
}


