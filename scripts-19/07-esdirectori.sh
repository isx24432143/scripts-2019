#! /bin/bash
# Jamison David Quelal León ASIX-M01
# Febrer 2020
# Si és un directori, fer un ls
#--------------------------------------
#Validar Argument
if [ $# -ne 1 ]; then
  echo "ERROR: num args incorrecte"
  echo "USAGE: $0 directori"
  exit 1
fi
#Validar si és un directori
if ! [ -d $1 ]; then
  echo "ERROR: $1 no és un directori"
  echo "USAGE: $0 directori"
  exit 2
fi
# Programa
dir=$1
ls $dir
exit 0
