#! /bin/bash
# Jamison David Quelal León ASIX-M01-ISO
# Febrer 2020
# Validar que es rep un argument i que és
# un directori i llistar-ne le contingut
#--------------------------------------
ERR_NARGS=1
ERR_NODIR=2
OK=0

#Validar Argument
if [ $# -eq 0 ]; then
	echo "ERROR: numero no vàlid">> /dev/stderr
	echo "USAGE: $0 dir[...]" >> /dev/stderr
	exit $ERR_NARGS
fi
# Posem un bucle, per poder fer-ho 
# per diferents directoris (cada arg) $*
for dir in $*
do
  #validar si és un directori
  if ! [ -d $dir ]; then 
   echo "ERROR: $dir no és un directori">>/dev/stderr
   echo "USAGE: $0 directori">>/dev/stderr
 else
  #3) Xixa
  echo "Processant directori: $dir"  
  fileList=$(ls $dir)
  num=1
  for file in $fileList
  do
    if [ -h $dir/$file ]; then
      echo -e "\t$num: $file és un Symbolic Link"
    elif [ -f $dir/$file ]; then
       echo -e "\t$num: $file és un fitxer regular"
    elif [ -d $dir/$file ]; then
       echo -e "\t$num: $file és un directori"
    else
       echo -e "\t$num: $file és una altra cosa"
     fi
     num=$((num+1))
     done
  fi
done
exit $OK
