#! /bin/bash
# Jamison David Quelal León
# Curs 2019-2020
# Març 2020
# funcions02
#--------------------------

#Mostrar stdIn numerat
function numeraStdIn(){
   num=1
   while read -r line
   do
     echo "$num: $line"
     ((num++))

   done
}
#numfile file
function numFile(){
   ERR_NARGS=1
   ERR_NOFILE=2
   if [ $# -ne 1 ]; then
      echo "ERROR: num args incorrecte"
      echo "USAGE: $0 file"
      return $ERR_NARGS
   fi
   #Validem si existeix el fitxer
   if [ ! -f "$1" ]; then
     echo "ERROR: $1 no és un fitxer"
     echo "USAGE: $0 file"
     return $ERR_NOFILE
   fi
   #Programa
   fileIn=$1
   num=1
   while read -r line
   do
     echo "$num: $line"
     ((num++))
   done < $fileIn
}

#numFileDefault [file]

function numFileDefault(){
   ERR_NARGS=1
   ERR_NOFILE=2
   if [ $# -gt 1 ]; then
     echo "ERROR: num args incorrecte"
     echo "USAGE: $0 [file]"
     return $ERR_NARGS
   fi
   #Validem si existeix el fitxer
   fileIn=/dev/stdin
   if [ $# -eq 1 ]; then
      if [ ! -f "$1" ]; then
        echo "ERROR: $1 no és un fitxer"
        echo "USAGE: $0 file"
	return $ERR_NOFILE
     fi
     fileIn=$1
   fi
   #Programa
   num=1
   while read -r line
   do
     echo "$num: $line"
     ((num++))
   done < $fileIn
   return 0
}

#filterGID file

function filterGid(){
   ERR_NARGS=1
   ERR_NOFILE=2
   #Validem nº argument
   if [ $# -ne 1 ]; then
      echo "ERROR: num args incorrecte"
      echo "USAGE: $0 file"
      return $ERR_NARGS
   fi
   #Validem file
   if [ ! -f "$1" ]; then
      echo "ERROR: $1 no és un fitxer"
      echo "USAGE: $0 file"
      return $ERR_NOFILE
   fi
   #Programa
   fileIn=$1
   while read -r line
   do
     gid=$(echo $line | cut -d: -f4)
     if [ $gid -ge 500 ]; then
	echo "$line"  | cut -d: -f1,3,4,6
    fi
   done < $fileIn
   return 0
}

#filterGID [file]
function filterGidDefault(){
   ERR_NARGS=1
   ERR_NOFILE=2
   if [ $# -gt 1 ]; then
      echo "ERROR: num args incorrecte"
      echo "USAGE: $0 [file]"
      return $ERR_NARGS
    fi
    #Validem si existeix el file
    fileIn=/dev/stdin
    if [ $# -eq 1 ]; then
       if [ ! -f "$1" ]; then
	  echo "ERROR: $1 no és un fitxer"
	  echo "USAGE: $0 [file]"
       fi
       fileIn=$1
    fi
    #programa
    while read -r line
    do
      gid=$(echo $line | cut -d: -f4)
      if [ $gid -ge 500 ]; then
	 echo $line | cut -d: -f1,3,4,6
      fi
      done < $fileIn
      return 0
}
