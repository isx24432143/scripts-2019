#! /bin/bash
# Jamison David Quelal León ASIX-M01-ISO
# Febrer 2020
# Exemples case
#--------------------------------------
# dl dt dc dj dv ds dm
case $1 in
  "dl"|"dt"|"dc"|"dj"|"dv")
   echo "$1 és dun dia laborable";;
  "ds"|"dm")
   echo "$1 és festiu";;
  *)
   echo "($1) no és un dia"
esac
exit 0

case $1 in
  [aeiou])
    echo "$1 és una vocal";;
   [bcdfghjklmnpqrstvwxyz])
    echo "$1 és una consonant";;
   *)
    echo "$1 és una altra cosa";;
esac
exit 0

case $1 in
  "pere"|"pau"|"joan")
   echo "és un nen";;
  "marta"|"anna"|"júlia")
   echo "és una nena";;
   *)
   echo "Indefinit";;
esac
exit 0
