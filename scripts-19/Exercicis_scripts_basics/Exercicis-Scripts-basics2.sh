#! /bin/bash
# Jamison David Quelal León ASIX-M01-ISO
# Febrer 2020
# 2. Mostar els arguments rebuts línia a línia, tot numerànt-los.
#------------------------------------------------------
ERR_ARGS=1
#Validar arguments
if [ $# -eq 0 ]; then
  echo "ERROR: num args no vàlid"
  echo "USAGE: $0 args[...]"
  exit $ERR_ARGS
fi
#Programa
num=1
for arg in $*
do
  echo "$num: $arg"
  num=$((num+1))
done
exit 0
