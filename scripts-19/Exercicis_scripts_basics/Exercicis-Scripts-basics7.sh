#! /bin/bash
# Jamison David Quelal León ASIX-M01-ISO
# Febrer 2020
# Processar línia a línia l'entrada estàndard
# Si la línia té més de 60 caràcters la mostra, si no no
#------------------------------------------------------
FI=0
#Programa
while read -r line
do
  num=$(echo "$line" | wc -L)
  if [ $num -gt 60 ]; then
   echo "$line"
  fi
done
exit $FI
