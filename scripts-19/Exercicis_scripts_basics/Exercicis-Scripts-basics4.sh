#! /bin/bash
# Jamison David Quelal León ASIX-M01
# Febrer 2020
# 4. Fer un programa que rep com a arguments números de més (un o més) 
# i indica per a cada mes rebut quants dies té el més.
#---------------------------------------------
ERR_ARGS=1
FI=0
#Validar numero args
if [ $# -eq 0 ]; then
  echo "ERROR: Numero args no vàlid"
  echo "USAGE: $0 num[...]"
  exit $ERR_ARGS
fi

for mes in $*
do
  if ! [ $mes -ge 1 -a $mes -le 12 ]; then
    echo "ERROR: $mes no vàlid" >> /dev/stderr
    echo "USAGE: $0 mes[1-12]" >> /dev/stderr
  else
    case $mes in
      "1"|"3"|"5"|"7"|"8"|"10"|"12")
       echo "El mes $mes té 31 dies";;
      "4"|"6"|"9"|"11")
       echo "El mes $mes té 30 dies";;
      "2")
       echo "El mes $mes té 28 dies";;
    esac
  fi
done
exit $FI

