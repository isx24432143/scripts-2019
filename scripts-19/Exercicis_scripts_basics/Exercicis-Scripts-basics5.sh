#! /bin/bash
# Jamison David Quelal León ASIX-M01-ISO
# Febrer 2020
# 5. Mostrar línia per línia l'entrada estàndard, retallant
# Només els primers 50 caràcters
#------------------------------------------------------
FI=0
#Programa
while read -r line
do
  echo "$line" | cut -c1-50
done
exit $FI
