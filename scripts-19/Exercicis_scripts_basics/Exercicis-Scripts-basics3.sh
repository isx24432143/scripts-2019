#! /bin/bash
# Jamison David Quelal León ASIX-M01
# Febrer 2020
# 3. Fer un comptador des de zero fins al valor indicat per l̉argument rebut.
#---------------------------------------------------
ERR_ARGS=1
OK=0
#Validar Argument
if [ $# -ne 1 ]; then
  echo "ERROR: num args incorrecte"
  echo "USAGE: $0 numero"
  exit $ERR_ARGS
fi
#Programa
num=0
final=$1
while [ $num -le $final ]
do
  echo $num
  num=$((num+1))
done
exit $OK
