#! /bin/bash
# Jamison David Quelal León ASIX-M01-ISO
# Febrer 2020
# 8. Fer un programa que rep com a argument noms d̉usuari, s
# si existeixen en el sistema (en el fitxer /etc/passwd) mostra el nom per stdout. 
# Si no existeix el mostra per stderr.
#------------------------------------------------------
ERR_ARGS=1
FI=0
#validar Argument
if [ $# -eq 0 ]; then
  echo "ERROR: nº args incorrecte"
  echo "USAGE: $0 noms_usuari[...]"
  exit $ERR_ARGS
fi
#Programa
usuaris=$*
for user in $usuaris
do
  grep "^$user:" /etc/passwd &> /dev/null
  if [ $? -eq 0 ]; then
    echo $user 
  else
    echo $user >> /dev/stderr
  fi
done
exit $FI
