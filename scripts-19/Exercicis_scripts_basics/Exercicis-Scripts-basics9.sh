#! /bin/bash
# Jamison David Quelal León ASIX-M01-ISO
# Febrer 2020
# 9. Fer un programa que rep per stdin noms d̉usuari (un per línia), 
# si existeixen en el sistema (en el fitxer /etc/passwd) mostra el nom per stdout. 
# Si no existeix el mostra per stderr.
#-------------------------------------------------------------------
FI=0
#Programa
while read -r line
do
  grep "^$line:" /etc/passwd &> /dev/null
  if [ $? -eq 0 ]; then
    echo $line
  else
    echo $line >> /dev/stderr
  fi
done
exit $FI
