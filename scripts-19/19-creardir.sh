#! /bin/bash
# Jamison David Quelal León ASIX-M01-ISO
# Febrer 2020
# prog nomdir[...]
# Validar si existeix almenys un argument
# Fer que la ordre mkdir no generi cap mena sortida
# Fer nosaltres mateixos la sortida per >>/dev/stderr
#--------------------------------------------------
#Validar Argument
ERR_MKDIR=2
ERR_ARGS=1
status=0
if [ $# -lt 1 ]; then
  echo "ERROR: num args incorrecte"
  echo "USAGE: $0 nomdir[...]"
  exit $ERR_ARGS
fi

#Programa
for nom in $*
do
  mkdir $nom &> /dev/null
  if [ $? -ne $STATUS]; then
   echo "ERROR: No crear $nom" >> /dev/stderr
   status=$ERR_MKDIR
  fi
done
exit $status
