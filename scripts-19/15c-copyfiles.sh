#! /bin/bash
# Jamison David Quelal León ASIX-M01-ISO
# Febrer 2020
# Donat un arg: file i 2arg: dir, copia el file al directori
#--------------------------------------
ERR_NARGS=1
ERR_NODIR=2
ERR_NOREGFILE=3
OK=0

#Validar Argument
if [ $# -lt 2 ]; then
  echo "ERROR: num args incorrecte"
  echo "USAGE: $0 file[...] dir-destí"
  exit $ERR_NARGS
fi
desti=$(echo $* | cut -d' ' -f$#)
llistaFile=$(echo $* | cut -d' ' -f1-$(($#-1)))
#Validar si és un directori
if ! [ -d $desti ]; then
  echo "ERROR: $desti no és un directori"
  echo "USAGE: $0 file dir-destí"
  exit 2
fi

#XIXA
for file in $llistaFile
do
  if [ ! -f $file ]; then
   echo "ERROR: $file no és un regular file" >>/dev/stderr
   echo "USAGE: $0 file[...] dir-destí" >> /dev/stderr
  fi
  cp $file $desti
done
exit $OK
