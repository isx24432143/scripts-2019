#! /bin/bash
# Jamison David Quelal León ASIX-M01
# Febrer 2020
# valida argument ex04
#-------------------------
#validar Argument
if [ $# -ne 2 ]; then
	echo "Error: n# arguments incorrecte"
	echo "Usage: $0: arg1 arg2"
	exit 1
fi
#Mostra arguments
echo "Els arguments son: $1, $2"
exit 0
