#! /bin/bash
# Jamison David Quelal León ASIX-M01-ISO
# Febrer 2020
# prog -a file -b -c -d num -f arg[...]
#--------------------------------------
#Validar Argument
ERR_NARGS=1
OK=0
if [ $# -lt 1 ]; then
  echo "ERROR: num args incorrecte"
  echo "USAGE: $0 [-a -b -c -d -f] args[...]"
  exit $ERR_ARGS
fi
#Programa
opcions=""
arguments=""
file=""
edat=""
while [ -n "$1" ]
do
  case $1 in
   "-b"|"-c"|"-f")
    opcions="$opcions $1";;
   "-a")
    opcions="$opcions $1"
    file=$2
    shift;;
   "-d")
    opcions="$opcions $1"
    edat=$2
    shift;;
    *)
    arguments="$arguments $1";;
  esac
shift
done

echo "Opcions: $opcions"
echo "Arguments: $arguments"
echo "Files: $file"
echo "Edat: $edat"
exit $OK
