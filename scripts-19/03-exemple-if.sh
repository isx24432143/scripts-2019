#! /bin/bash
# Jamison David Quelal León ASIX-M01
# Gener 2020
# Exemple if
#  $ prog edat
#--------------------------------------
# Validar argument
if [ ! $# -eq 1 ]; then
	echo "Error: n# arguments incorrecte"
	echo "Usage: $0 edat"
	exit 1
fi
# xixa
edat=$1
if [ $edat -ge 18 ]; then
	echo "edat $edat és major d'edat"
fi
exit 0
