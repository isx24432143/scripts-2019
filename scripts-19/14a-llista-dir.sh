#! /bin/bash
# Jamison David Quelal León ASIX-M01-ISO
# Febrer 2020
# Validar que es rep un argument i que és
# un directori i llistar-ne le contingut
#--------------------------------------
ERR_NARGS=1
ERR_NODIR=2

#Validar Argument
if [ $# -ne 1 ]; then
  echo "ERROR: num args incorrecte"
  echo "USAGE: $0 directori"
  exit $ERR_NARGS
fi
dir=$1
#Validar si és un directori
if ! [ -d $dir ]; then 
  echo "ERROR: $dir no és un directori"
  echo "USAGE: $0 directori"
  exit $ERR_NODIR
fi
#programa
ls $dir
