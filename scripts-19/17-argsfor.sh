#! /bin/bash
# Jamison David Quelal León ASIX-M01-ISO
# Febrer 2020
# Donat args[..], separar els arguments i les opcions
#----------------------------------------------------------
#Validar Argument
ERR_ARGS=1
OK=0
if [ $# -lt 0 ]; then
  echo "ERROR: num args incorrecte"
  echo "USAGE: $0 [-a -b -c -d -e -f] args[...]"
  exit $ERR_ARGS
fi
#Programa
opcions=""
arguments=""
for arg in $*
do
  case $arg in
   "-a"|"-b"|"-c"|"-d"|"-e"|"-f")
    opcions="$opcions $arg";;
   *)
    arguments="$arguments $arg";;
  esac
done
echo "Opcions: $opcions"
echo "Arguments: $arguments"
exit $Ok
  
