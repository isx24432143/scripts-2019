#! /bin/bash
# Jamison David Quelal León isx24432143 ASIX-M01-ISO
# Febrer 2021
# Donat un arg: file i 2arg: dir, copia el file al directori
#--------------------------------------
ERR_NARGS=1
ERR_NOFILE=2
ERR_NODIR=3
status=0

#Validar nº arguments
if [ $# -ne 2 ]; then
  echo "ERROR: num args incorrecte"
  echo "USAGE: $0 file dir-destí"
  exit $ERR_NARGS
fi

#Validar si $1 existeix
if [ ! -e $1 ]; then
  echo "ERROR: $1 no existeix"
  echo "USAGE: $0 file dir-destí"
  exit $ERR_NOFILE
fi

#3) Verificar si $2 és un directory i existeix
if [ ! -d $2 ]; then
  echo "ERROR: $2 no és un Directori"
  echo "USAGE: $0 file dir-destí"
  exit $ERR_NODIR
fi

#Programa
file=$1
dir=$2
cp $file $dir
exit $status
