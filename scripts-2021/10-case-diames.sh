#! /bin/bash
# Jamison David Quelal León isx24432143 ASIX-M01-ISO
# Febrer 2021
# Case, si el mes té 28,30 o 31 dies
# $ prog [1-12]
#--------------------------------------
#Validar Argument
ERR_NARGS=1
ERR_ARGVL=2
OK=0
if [ $# -ne 1 ]; then
  echo "ERROR: num args incorrecte"
  echo "USAGE: $0 mes"
  exit $ERR_NARGS
fi

if [ "$1" = "-h" -o "$1" = "--help" ]; then
  echo "Escola del Treball"
  echo "Jamison David Quelal León ASIX-M01-ISO"
  echo "USAGE: $0 [1-12]"
  exit $OK
fi
# Si el mes està amb numeros, sortir
mes=$1
if ! [ $mes -ge 1 -a $mes -le 12 ]; then
  echo "ERROR: mes no valid"
  echo "Mes pren valors del [1-12]"
  echo "USAGE: $0 mes"
  exit $ERR_ARGVL
fi

#Programa
case $mes in
  "1"|"3"|"5"|"7"|"8"|"10"|"12")
   echo "El mes $mes té 31 dies";;
  "4"|"6"|"9"|"11")
   echo "El mes $mes té 30 dies";;
  "2")
   echo "El $mes té 28 dies";;
esac
exit $OK
