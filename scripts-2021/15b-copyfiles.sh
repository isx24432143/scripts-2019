#! /bin/bash
# Jamison David Quelal León isx24432143 ASIX-M01-ISO
# Febrer 2021
# Donat un arg: file i 2arg: dir, copia el file al directori
# $prog.sh file[...] dir-destí
#--------------------------------------
ERR_NARGS=1
ERR_NODIR=2
ERR_NOFILE=3
status=0

#Validar Argument
if [ $# -lt 2 ]; then
  echo "ERROR: num args incorrecte"
  echo "USAGE: $0 file[...] dir-destí"
  exit $ERR_NARGS
fi
desti=$(echo $* | sed 's/^.* //')
llistaFile=$(echo $* | sed 's/ [^ ]*$//')
#Validar si és un directori
if [ ! -d $desti ]; then
  echo "ERROR: $2 no és un directori"
  echo "USAGE: $0 file[...] dir-destí"
  exit $ERR_NODIR
fi

#XIXA
for file in $llistaFile
do
  if [ ! -e $file ]; then
   echo "ERROR: $file no existeix" >>/dev/stderr
   echo "USAGE: $0 file[...] dir-destí" >> /dev/stderr
  fi
  cp $file $desti
done
exit $status
