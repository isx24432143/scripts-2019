#! /bin/bash
# Jamison David Quelal León ASIX-M01-ISO
# Febrer 2021
# Descripció: Exemples del bucle while
#--------------------------------------

# 8) Processar l'entrada estandard fins el token "FI"
num=1
while read -r line 
do
  echo "$num: $line" | tr '[:lower:]' '[:upper:]'
  ((num++))
done
exit 0

# Mostrar l'entrada estandard numerada
num=1
while read -r line
do
  echo "$num: $line"
  ((num++))
done
exit 0

# Mostrar la entrada estandard línia a línia

while read -r line
do
  echo $line
done
exit 0


#Mostra els arguments, mentre hi hagi xixa a $1, amb numerant
num=1
while [ $# -gt 0 ]
do
  echo "$num: $1, $#, $*"
  ((num++))
  shift
done
exit 0



#Mostrar tot el que rep per stdin, però passat a majúscules
#	I numerant les línies

num=1
while read -r line
do
	echo "$num $line" | tr '[:lower:]' '[:upper:]'
	num=$((num+1))
done
exit 0

# 8) Processar l'entrada estandard fins el token "FI"

read -r line
while [ $line != "FI" ]
do
  echo "$line"
  read -r line
done
exit 0


# 07) Mostrar l'entrada estandard numerada
num=1
while read -r line
do
  echo "$num: $line"
  num=$((num+1))
done
exit 0

# 6) Mostrar la entrada estandard línia a línia

while read -r line
do
  echo $line
done
exit 0

#Mostra els arguments, mentre hi hagi xixa a $1, amb numerant
num=1
while [ -n "$1" ]
do
  echo "$num: $1, $#, $*"
  num=$((num+1))
  shift
done
exit 0

# Mostrar els arguments i desplaça mentre a $1 hi hagi xixa
while [ -n "$1" ]
do
  echo "$1 $#: $*"
  shift
done
exit 0

# Mostrar els arguments
while [ $# -gt 0 ]
do
  echo "$#: $*"
  shift
done
exit 0


# comptador decreixent del arg rebut
MINIM=0
num=$1
while [ $num -ge $MINIM ]
do
  echo -n "$num, "
  num=$((num-1))
done
exit 0

#Mostrar del [1-10]
MAX=10
num=0
while [ $num -le $MAX ] 
do
  num=$((num+1))	
  echo "$num"
done
exit 0
