#! /bin/bash
# Escola del Treball ASIX M01-ISO
# Jamison León isx24432143
# 07/04/2021
#-----------------------------------

function getSize(){
  status=0
  homedir=$1
  if [ -d "$homedir" -a $? -eq 0 ]; then
   du -bs $homedir | tr -s '[:blank:]' ':' | cut -d: -f1
  else
   status=1
  fi
  return $status
}


function getHoleList(){
  status=0
  ERR_ARGS=1
  #Validem el Nº Args
  if [ $# -eq 0 ]; then
   echo "ERROR: Nº Args Incorrecte"
   echo "USAGE: $0 login[..]"
   return $ERR_ARGS
  fi
  #Programa
  for user in $*
  do
    linia=$(grep "^$user:" /etc/passwd)
    if [ -z "$linia" ]; then
      status=2
    else
      getHome $user
    fi
  done
  return $status

}


function getHome(){
  status=0 
  login=$1
  grep "^$login:" /etc/passwd &> /dev/null
  if [ $? -ne 0 ]; then
    status=1
  else
    grep "^$login:" /etc/passwd | cut -d: -f6
  fi
  return $status

}



function creaEscola(){
  for classe in $*
  do
    creaClasse $classe
  done
}


function creaClasse(){
  classe=$1
  PASSWD="alum"
  llista_noms=$(echo ${classe}{01..02})
  for user in $llista_noms
  do	  
    useradd $user
    echo "$user:$PASSWD" | chpasswd
    #chpassd < file
    #echo "alum" | passwd --stdin $user &> /dev/null
  done
}



#showGroupMainMembers gname

function showAllGroups(){
  MIN_USERS=2
  llista_gids=$(cut -d: -f4 /etc/passwd | sort -n | uniq)
  for gid in $llista_gids
   do
     count=$(grep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd | wc -l)
     if [ $count -ge $MIN_USERS ]; then
	gname=$(grep "^[^:]*:[^:]*:$gid:" /etc/group | cut -d: -f1)
        echo "$gname($gid): $count"
	grep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd | cut -d: -f1,3,7 | sort | tr '[[:lower:]]' '[[:upper:]]' | sed -r 's/^(.*)$/\t\1/'
     fi
done
}

