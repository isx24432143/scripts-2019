#! /bin/bash
# Jamison David Quelal León
# Curs 2019-2020
# Març 2020
#--------------------------

function hola(){
  echo "Hola"
  return 0
}

function dia(){
  date
  return 0
}

function suma(){
  echo $(($1+$2))
  return 0
}

#showUser(login)
#Mostrar un a un els camps amb label
function showUser(){
   status=0
   ERR_NARGS=1
   ERR_NOLOGIN=2
   if [ $# -ne 1 ]; then
    echo "ERROR: num args incorrecte"
    echo "USAGE: $0 login"
    return $ERR_NARGS
  fi
  #2) validar existeix login
  login=$1
  line=""
  line=$(grep "^$login:" /etc/passwd 2> /dev/null)
  if [ -z "$line" ]; then
    echo "ERROR: login $login inexist"
    echo "USAGE: $0 login"
    return $ERR_NOLOGIN
  fi
  #3) Mostrar
  uid=$(echo "$line" | cut -d: -f3)
  gid=$(echo "$line" | cut -d: -f4)
  gecos=$(echo "$line" | cut -d: -f5)
  home=$(echo "$line" | cut -d: -f6)
  shell=$(echo "$line" | cut -d: -f7)
  echo "Login: $login"
  echo "UID: $uid"
  echo "GID: $gid"
  echo "GECOS: $gecos"
  echo "HOME: $home"
  echo "SHELL: $shell"
  return $status
}

#showUserGecos login
function showUserGecos(){
   ERR_NARGS=1
   ERR_NOLOGIN=2
   FI=0
   #1) validar argument
   if [ $# -ne 1 ]; then
    echo "ERROR: num args incorrecte"
    echo "USAGE: $0 login"
    return $ERR_NARGS
   fi
   #2)validar login
   login=$1
   line=""
   line=$(grep "^$login:" /etc/passwd 2> /dev/null)
   if [ -z "$line" ]; then
    echo "ERROR: login $login inexistent"
    echo "USAGE: $0 login"
    return $ERR_NOLOGIN
   fi
   #3) Mostrar
   gecos=$(echo "$line" | cut -d: -f5)
   name=$(echo "$gecos" | cut -d, -f1)
   office=$(echo "$gecos" | cut -d, -f2)
   telefOffice=$(echo "$gecos" | cut -d, -f3)
   telefHome=$(echo "$gecos" | cut -d, -f4)
   echo "Nom: $name"
   echo "Oficina: $office"
   echo "Telèfon oficina: $telefOffice"
   echo "Telèfon Home: $telefHome"
   return $FI
}

#showGroup gname
function showGroup(){
   ERR_NARGS=1
   ERR_NOGNAME=2
   status=0
   #1) validar argument
   if [ $# -ne 1 ]; then
     echo "ERROR: num args incorrecte"
     echo "USAGE: $0 gname"
     return $ERR_NARGS
   fi
   #2)validar gname
   gname=$1
   line=""
   line=$(grep "^$gname:" /etc/group 2> /dev/null)
   if [ -z "$line" ]; then
     echo "ERROR: gname $gname inexistent"
     echo "USAGE: $0 gname"
     return $ERR_NOGNAME
   fi
   #3) Mostrar
   gid=$(echo $line | cut -d: -f3)
   userList=$(echo $line | cut -d: -f4)
   echo "GNAME: $gname"
   echo "GID: $gid"
   echo "UserList: $userList"
   return $status
}

#showUser+gname

function showUserGname(){
   ERR_NARGS=1
   ERR_NOLOGIN=2
   if [ $# -ne 1 ]; then
    echo "ERROR: num args incorrecte"
    echo "USAGE: $0 login"
    return $ERR_NARGS
   fi
   #2) validar existeix login
   login=$1
   line=""
   line=$(grep "^$login:" /etc/passwd 2> /dev/null)
   if [ -z "$line" ]; then
    echo "ERROR: login $login inexist"
    echo "USAGE: $0 login"
    return $ERR_NOLOGIN
   fi
   #3) Mostrar
   uid=$(echo "$line" | cut -d: -f3)
   gid=$(echo "$line" | cut -d: -f4)
   gname=$(grep "^[^:]*:[^:]*:$gid:" /etc/group | cut -d: -f1)
   gecos=$(echo "$line" | cut -d: -f5)
   home=$(echo "$line" | cut -d: -f6)
   shell=$(echo "$line" | cut -d: -f7)
   echo "Login: $login"
   echo "UID: $uid"
   echo "GNAME: $gname"
   echo "GID: $gid"
   echo "GECOS: $gecos"
   echo "HOME: $home"
   echo "SHELL: $shell"
   return $FI								 
}

#showUserList login[...]

function showUserList(){
  status=0
  FI=0
  if [ $# -eq 0 ]; then
    echo "ERROR: num args incorrecte"
    echo "USAGE: $0 login[...]"
    status=1
    return $status
  fi
  for login in $*
  do
    #2) validar existeix login
    login1=$login
    line=""
    line=$(grep "^$login1:" /etc/passwd 2> /dev/null)
    if [ -z "$line" ]; then
      echo "ERROR: login $login inexist" >> /dev/stderr
      echo "USAGE: $0 login[...]" >> /dev/stderr
      status=2
    else
      #3) Mostrar
      uid=$(echo "$line" | cut -d: -f3)
      gid=$(echo "$line" | cut -d: -f4)
      gname=$(grep "^[^:]*:[^:]*:$gid:" /etc/group | cut -d: -f1)
      gecos=$(echo "$line" | cut -d: -f5)
      home=$(echo "$line" | cut -d: -f6)
      shell=$(echo "$line" | cut -d: -f7)
      echo "Login: $login"
      echo "UID: $uid"
      echo "GNAME: $gname"
      echo "GID: $gid"
      echo "GECOS: $gecos"
      echo "HOME: $home"
      echo "SHELL: $shell"
      status=0
fi
done
return $status
}

#showUserIn < filein

function showUserIn(){
  status=0
  while read -r login
  do
    linia=$(grep "^$login:" /etc/passwd 2> /dev/null)
    if [ -z "$linia" ]; then
      echo "ERROR: login $login inexist" >> /dev/stderr
      echo "USAGE: $0 login[...]" >> /dev/stderr
      status=1
    else
     uid=$(echo "$linia" | cut -d: -f3)
     gid=$(echo "$linia" | cut -d: -f4)
     gname=$(grep "^[^:]*:[^:]*:$gid:" /etc/group | cut -d: -f1)
     gecos=$(echo "$linia" | cut -d: -f5)
     home=$(echo "$linia" | cut -d: -f6)
     shell=$(echo "$linia" | cut -d: -f7)
     echo "Login: $login"
     echo "UID: $uid"
     echo "GNAME: $gname"
     echo "GID: $gid"
     echo "GECOS: $gecos"
     echo "HOME: $home"
     echo "SHELL: $shell"
     status=0
fi	   
done
}

#showGroupMainMembers gname

function showGroupMainMembers(){
  ERR_NARGS=1
  ERR_NOGNAME=2
  if [ $# -ne 1 ]; then
   echo "ERROR: num args incorrecte"
   echo "USAGE: $0 gname"
   return $ERR_NARGS
  fi
  #2) Validem gname existeix
  gname=$1
  gid=""
  gid=$(grep "^$gname:" /etc/group | cut -d: -f3)
  if [ -z "$gid" ]; then
   echo "ERROR: gname $gname inexist"
   echo "USAGE: $0 gname"
   return $ERR_NOGNAME
  fi
  #3) Filtrar
  echo "llistat grup: $gname($gid)"
  grep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd | cut -d: -f1,3,6,7 | sort -k2g,2 | tr '[a-z]' '[A-Z]' | sed 's/:/  /g' | sed -r 's/^(.*)$/\t\1/'  
  return 0
}

#showAllShells
function showAllShells(){
  llistaShells=$(cut -d: -f7 /etc/passwd | sort | uniq)
  for shell in $llistaShells
   do
    num=$(grep ":$shell$" /etc/passwd | wc -l)  
    if [ $num -gt 2 ]; then
       echo "$shell ($num):"
       grep ":$shell$" /etc/passwd | cut -d: -f1,3,4,6 | sort -k2g,2 | sed -r 's/^(.*)$/\t\1/g'
    fi
  done
}
