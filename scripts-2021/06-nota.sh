#! /bin/bash
# Jamison David Quelal León ASIX-M01
# Febrer 2021
# Validar Nota, Si està suspès, aprovat, notable, excel·lent
#--------------------------------------
ERR_NARGRS=1
ERR_VALOR=2

#Validar Argument
if [ $# -ne 1 ]; then
  echo "ERROR: num args incorrecte"
  echo "USAGE: $0 nota"
  exit $ERR_NARGS
fi
# Si nota no és [0-10], sortir
if ! [ $1 -ge 0 -a $1 -le 10 ]; then
  echo "ERROR: nota $1 no valida [0-10]"
  echo "USAGE: $0 nota"
  exit $ERR_VALOR
fi
# xixa
nota=$1
if [ $nota -lt 5 ]; then
  echo "Suspès"
elif [ $nota -lt 7 ]; then
  echo "Aprovat"
elif [ $nota -lt 9 ]; then
  echo "Notable"
else
  echo "Execel·lent"
fi
exit 0
