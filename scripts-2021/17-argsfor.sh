#! /bin/bash
# Jamison David Quelal León isx24432143 ASIX-M01-ISO
# Febrer 2021
# Donat args[..], separar els arguments i les opcions
#----------------------------------------------------------
ERR_ARGS=1
status=0

#Validem el nº args
if [ $# -eq 0 ]; then
  echo "ERROR: num args incorrecte"
  echo "USAGE: $0 [-a -b -c -d -f -g] args[...]"
  exit $ERR_ARGS
fi
#Programa
opcions=""
arguments=""
for arg in $*
do
  case $arg in
   "-a"|"-b"|"-c"|"-d"|"-f"|"-g")
    opcions="$opcions $arg";;
   *)
    arguments="$arguments $arg";;
  esac
done
echo "Opcions: $opcions"
echo "Arguments: $arguments"
exit $status
