#! /bin/bash
# Jamison David Quelal León isx24432143 ASIX-M01
# Febrer 2021
# Si és un file, dir si és un fitxer regular, dir, o un link etc...
# $ prog fit
#--------------------------------------
#Validar Argument
ERR_NARGS=1
ERR_NOEL=2
if [ $# -ne 1 ]; then
  echo "ERROR: num args incorrecte"
  echo "USAGE: $0 fit"
  exit $ERR_ARGS
fi
#Validar si el fitxer existeix
if ! [ -e $1 ]; then
  echo "Error: $1 no existeix"
  echo "Usage: $0 fit"
  exit $ERR_NOEL
fi
#Programa
fit=$1
if [ -h $fit ]; then
  echo "$fit és un Symbolic Link"
elif [ -f $fit ]; then
  echo "$fit és un fitxer regular"
elif [ -d $fit ]; then
  echo "$fit és un directori"
else
  echo "$fit és una altra cosa"
fi
exit 0
