#! /bin/bash
# Jamison David Quelal León ASIX-M01
# Gener 2020
# Exemple if
#  $ prog edat
#--------------------------------------
# Validar argument
status=0
if [ ! $# -eq 1 ]; then
       echo "Error: n# arguments incorrecte"
       echo "Usage: $0 edat"
       status=1
       exit $status
fi
# xixa
edat=$1
if [ $edat -ge 18 ]; then
   echo "edat $edat és major d'edat"
   status=0
else
   echo "edat $edat no és major d'edat"
   status=2
fi
exit $status
