#! /bin/bash
# Jamison David Quelal León isx24432143 :ASIX-M01-ISO
# Febrer 2021
# 9. Fer un programa que rep per stdin noms d̉e grups (un per línia), 
# si existeixen en el sistema (en el fitxer /etc/group) mostra el nom per stdout. 
# Si no existeix el mostra per stderr.
# $ prog.sh nom_grups[...]
#-------------------------------------------------------------------
ERR_NARGS=1
status=0

#Validem el nº d'args
if [ $# -eq 0 ]; then
  echo "ERROR: Nº Args incorrecte"
  echo "USAGE: $0 nom_grups[...]"
  exit $ERR_NARGS
fi
#Programa
grups=$*
for grup in $grups
do
  grep "^$grup:" /etc/group &> /dev/null
  if [ $? -eq 0 ]; then
     echo "$grup"
  else
     echo "$grup" >> /dev/stderr
  fi
done
exit $status
