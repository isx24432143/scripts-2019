#! /bin/bash
# Jamison David Quelal León isx24432143 ASIX-M01-ISO
# Febrer 2021
# 1. Mostrar l̉entrada estààndard numerant línia a línia
#------------------------------------------------------
OK=0
#Programa
num=1
while read -r line
do
  echo "$num: $line"
  num=$((num+1))
done
exit $OK
