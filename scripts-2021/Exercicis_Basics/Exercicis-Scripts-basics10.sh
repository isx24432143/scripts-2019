#! /bin/bash
# Jamison David Quelal León ASIX-M01-ISO
# Febrer 2020
# 10. Fer un programa que rep com a argument un número indicatiu del número màxim de línies a mostrar. 
# El programa processa stdin línia a línia i mostra numerades un màxim de num línies.
#----------------------------------------------------
ERR_ARGS=1
FI=0
#Validar arguments
if [ $# -ne 1 ]; then
   echo "ERROR: numero args no vàlid"
   echo "USAGE: $0 num"
   exit $ERR_ARGS
fi
#Programa
maxim=$1
numlinia=1
read -r line
while [ $numlinia -le $maxim ]
do
  echo "$numlinia: $line"
  numlinia=$((numlinia+1))
  read -r line
done
exit $FI
