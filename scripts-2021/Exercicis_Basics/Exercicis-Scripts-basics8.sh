#! /bin/bash
# Jamison David Quelal León isx24432143 ASIX-M01-ISO
# Febrer 2021
# 8. Fer un programa que rep com a argument noms d̉usuari, s
# si existeixen en el sistema (en el fitxer /etc/passwd) mostra el nom per stdout. 
# Si no existeix el mostra per stderr.
# $ prog.sh noms_usuaris[...]
#------------------------------------------------------
ERR_ARGS=1
status=0
#validar Argument
if [ $# -eq 0 ]; then
  echo "ERROR: nº args incorrecte"
  echo "USAGE: $0 noms_usuari[...]"
  exit $ERR_ARGS
fi
#Programa
usuaris=$*
for user in $usuaris
do
  grep "^$user:" /etc/passwd &> /dev/null
  if [ $? -eq 0 ]; then
    echo $user 
  else
    echo $user >> /dev/stderr
  fi
done
exit $status
