#! /bin/bash
# Jamison David Quelal León isx24432143 ASIX-M01-ISO
# Febrer 2021
# Processar línia a línia l'entrada estàndard
# Si la línia té més de 60 caràcters la mostra, si no no
#------------------------------------------------------
status=0
#Programa
while read -r line
do
  num=$(echo "$line" | wc -m)
  if [ $num -gt 60 ]; then
   echo "$line"
  fi
done
exit $status
