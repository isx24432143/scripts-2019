#! /bin/bash
# Jamison David Quelal León isx24432143 ASIX-M01
# Febrer 2021
# 6. Fer un programa que rep com a arguments noms de dies de la setmana
# i mostrar quants dies eren laborables i quants festius.
# Si l'argument no és un dia de la setmana genera un erros per stderr
#---------------------------------------------
ERR_ARGS=1
status=0
#Validar numero args
if [ $# -eq 0 ]; then
  echo "ERROR: Numero args no vàlid"
  echo "USAGE: $0 dies_setmana"
  exit $ERR_ARGS
fi
#Programa
laborables=0
festius=0
for dia in $*
do
  case $dia in
    "dilluns"|"dimarts"|"dimecres"|"dijous"|"divendres")
     ((laborables++));;
    "dissabte"|"diumenge")
     ((festius++));;
    *)
     echo "ERROR: $dia no és un dia vàlid" >> /dev/stderr
     echo "USAGE: $0 dies_setmana" >> /dev/stderr;;
  esac
done
echo "$laborables dies laborables"
echo "$festius dies festius"
exit $status
