#! /bin/bash
# Jamison David Quelal León isx24432143 ASIX-M01-ISO
# Febrer 2021
# 5. Mostrar línia per línia l'entrada estàndard, retallant
# Només els primers 50 caràcters
#------------------------------------------------------
status=0
#Programa
while read -r line
do
  echo "$line" | cut -c1-50
done
exit $status
