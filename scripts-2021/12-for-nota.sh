#! /bin/bash
# Jamison David Quelal León ASIX-M01-ISO
# Febrer 2021
# prog nota[...]
#--------------------------------------
#Validar si té 1 o més arguments
ERR_NARGS=1
OK=0
if [ $# -lt 1 ]; then
  echo "ERROR: num args incorrecte"
  echo "USAGE: $0 nota"
  exit $ERR_NARGS
fi
#validar si l'argument és [0-10]
llistaNotes=$*
for nota in $llistaNotes
do
  if ! [ $nota -ge 0 -a $nota -le 10 ]; then
   echo "ERROR: la nota $nota no és vàlida" >> /dev/stderr
   echo "Valors vàlids 0-10" >> /dev/stderr
  elif [ $nota -lt 5 ]; then
   echo "$nota: Suspès"
  elif [ $nota -lt 7 ]; then
   echo "$nota: Aprovat"
  elif [ $nota -lt 9 ]; then
   echo "$nota: Notable"
  else
   echo "$nota: Execel·lent"
   fi 
done
exit $OK	
