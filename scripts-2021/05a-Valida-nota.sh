#! /bin/bash
# Jamison David Quelal León ASIX-M01
# Febrer 2021
# Validar Nota, Dir si està aprovat o no ho està
#--------------------------------------
#Validar Argument
if [ $# -ne 1 ]; then
	echo "ERROR: num args incorrecte"
	echo "USAGE: $0 nota"
	exit 1
fi
# Si nota no és [0-10], sortir
if ! [ $1 -ge 0 -a $1 -le 10 ]; then
	echo "ERROR: nota $1 no valida [0-10]"
	echo "USAGE: $0 nota"
	exit 2
fi
# xixa
nota=$1
if [ $nota -lt 5 ]; then
  echo "Suspès"
else
  echo "Aprovat"
fi
exit 0
