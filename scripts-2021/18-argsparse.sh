#! /bin/bash
# Jamison David Quelal León isx24432143 ASIX-M01-ISO
# Febrer 2021
# $ prog.sh -a file -b -c -d num -f arg[...]
#--------------------------------------
#Validar Argument
ERR_NARGS=1
status=0
if [ $# -lt 1 ]; then
  echo "ERROR: num args incorrecte"
  echo "USAGE: $0 [-a -b -c -d -f] args[...]"
  exit $ERR_NARGS
fi
#Programa
opcions=""
arguments=""
file=""
num=""
while [ -n "$1" ]
do
  case $1 in
   -[bcf])
    opcions="$opcions $1";;
   "-a")
    opcions="$opcions $1"
    file=$2
    shift;;
   "-d")
    opcions="$opcions $1"
    num=$2
    shift;;
    *)
    arguments="$arguments $1";;
  esac
shift
done

echo "File: $file"
echo "Num: $num"
echo "Opcions: $opcions"
echo "Arguments: $arguments"
exit $status
