# /bin/bash
# Jamison David Quelal León isx24432143 ASIX-M01-ISO
# Febrer 2021
# 2) Processar els arguments i comptar quantes n̉hi ha de 3 o més caràcters.
#-------------------------------------------
ERR_ARGS=1
status=0
#validar Arguments
if [ $# -eq 0 ]; then
  echo "ERROR: nº args no vàlid"
  echo "USAGE: $0 args[...]"
  exit $ERR_ARGS
fi
#Programa
compt=0
for arg in $*
do
  echo "$arg" | egrep ".{3,}" &> /dev/null
  if [ $? -eq 0 ]; then
   ((compt++))
  fi
done
echo $compt
exit $status
