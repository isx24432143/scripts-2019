# /bin/bash
# Jamison David Quelal León ASIX-M01-ISO
# Febrer 2020
# 5) Processar stdin mostrant per stdout les línies de menys de 50 caràcters.
#--------------------------------------------
FI=0
#Programa
while read -r line
do
  num=$(echo "$line" | wc -L)
  if [ $num -lt 50 ]; then
   echo $line
  fi
done
exit $FI
