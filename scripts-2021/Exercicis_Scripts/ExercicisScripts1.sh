# /bin/bash
# Jamison David Quelal León isx24432143 ASIX-M01-ISO
# Febrer 2021
# 1) Processar els arguments i mostrar per stdout només els de 4 o més caràcters.
#-----------------------------------------------
ERR_ARGS=1
status=0
#validar Arguments
if [ $# -eq 0 ]; then
  echo "ERROR: nº args no vàlid"
  echo "USAGE: $0 args[...]"
  exit $ERR_ARGS
fi
#Programa
for arg in $*
do
  echo "$arg" | egrep ".{4,}" &> /dev/null
  if [ $? -eq 0 ]; then
    echo $arg
  fi
done
exit $status
