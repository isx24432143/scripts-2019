# /bin/bash
# Jamison David Quelal León isx24432143 ASIX-M01-ISO
# Febrer 2021
# 4) Processar stdin cmostrant per stdout les línies numerades i en majúscules..
#-------------------------------------------
status=0
#Programa
numlinia=1
while read -r line
do
  echo "$numlinia: $line" | tr '[[:lower:]]' '[[:upper:]]'
  numlinia=$((numlinia+1))
done
exit $status
