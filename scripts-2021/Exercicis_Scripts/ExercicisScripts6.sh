# /bin/bash
# Jamison David Quelal León ASIX-M01-ISO
# Febrer 2020
# 6) Processar per stdin linies d̉entrada tipus “Tom Snyder” i mostrar per stdout la línia en format → T. Snyder.
#-------------------------------------------
FI=0
#Programa
while read -r line
do
  echo $line | sed -r 's/^([A-Z]{1})[^ ]* ([^ ]*)$/\1. \2/'  
done
exit $FI
