#! /bin/bash
# Jamison David Quelal León ASIX-M01-ISO
# Març 2020
# 10) Programa: prog.sh
# Rep per stdin GIDs i llista per stdout la informació de cada un d̉aquests grups, en format: gname: GNAME, gid: GID, users: USERS
#-------------------------------------------
#FI=0
while read -r line
do
  gid=$(egrep "^[^:]*:[^:]*:$line:" /etc/group)
  if [ $? -eq 0 ]; then
   gname=$(echo "$gid" | cut -d: -f1 | tr '[:lower:]' '[:upper:]')
   gid=$(echo "$gid" | cut -d: -f3)
   users=$(echo "$gid" | cut -d: -f4 | tr '[:lower:]' '[:upper:]')
   echo "gname: $gname"
   echo "gid: $gid"
   echo "users: $users"
  else
   echo "Error: gid $line inexistent" >> /dev/stderr
 fi
done
exit $FI
