#! /bin/bash
# Jamison David Quelal León isx24432143 ASIX-M01-ISO
# Febrer 2021
# 10) Programa: prog.sh
# Rep per stdin GIDs i llista per stdout la informació de cada un d’aquests grups, 
# en format: gname: GNAME, gid: GID, users: USERS
#----------------------------------------------
status=0
while read -r gid
do
  liniaGid=$(grep "^[^:]*:[^:]*:$gid:" /etc/group)
  if [ $? -eq 0 ]; then
   gname=$(echo $liniaGid | cut -d: -f1 | tr '[:lower:]' '[:upper:]')
   userList=$(echo $liniaGid | cut -d: -f4 | tr '[:lower:]' '[:upper:]')
   echo "gname: $gname gid: $gid users: $userList"
  else
   echo "ERROR: $gid inexistent" >> /dev/stderr
  fi
done
exit $status
