#! /bin/bash
# Jamison David Quelal León ASIX-M01-ISO
# Març 2020
# 12) Programa -h uid…
# Per a cada uid mostra la informació de l̉usuari en format:
# logid(uid) gname home shell
#------------------------------
FI=0
ERR_NARGS=1
#Validar arguments
if [ $# -eq 0 ]; then
  echo "ERROR: Nº args no vàlid"
  echo "USAGE: $0 uid[...:]"
  exit $ERR_NARGS
fi
#cas help
if [ $# -eq 1 -a "$1" = "-h" ]; then
  echo "Mostrant el help"
  exit $FI
fi
#Programa
status=0
for uid in $*
do
   ulinia=$(grep "^[^:]*:[^:]*:$uid:" /etc/passwd)
   if [ $? -eq 0 ]; then
     login=$(echo $ulinia | cut -d: -f1)
     gid=$(echo $ulinia | cut -d: -f4)
     gname=$(grep "^[^:]*:[^:]*:$gid:" /etc/group | cut -d: -f1)
     dirHome=$(echo $ulinia | cut -d: -f6)
     shell=$(echo $ulinia | cut -d: -f7)
     echo "$login($uid) $gname $dirHome $shell"
    else
     echo "ERROR: $uid inexistent" >> /dev/stderr
     status=2
   fi
done
exit $status
