#! /bin/bash
# Jamison David Quelal León ASIX-M01
# Febrer 2021
# primer exemple d'script
#-------------------------
status=0

echo "Hello World"
echo "Bon Dia, avui plou"
nom="Jamison Quelal León"
edat=22
echo $nom $edat
echo -e "nom: $nom\t edat: $edat"
echo -e 'nom: $nom\n edat: $edat'
echo $((edat*2))
ps
echo $SHLVL
read data1 data2
echo -e "$data1 \n $data2"
exit $status

