#! /bin/bash
# Jamison David Quelal León isx24432143 ASIX-M01
# Febrer 2021
# Si és un directori, fer un ls
# $prog.sh dir
#--------------------------------------
ERR_NARGS=1
ERR_NODIR=2

#Validar Argument
if [ $# -ne 1 ]; then
  echo "ERROR: num args incorrecte"
  echo "USAGE: $0 directori"
  exit $ERR_NARGS
fi
#Validar si el directori existeix
if ! [ -d $1 ]; then
  echo "ERROR: $1 no és un directori"
  echo "USAGE: $0 directori"
  exit $ERR_NODIR
fi
# Programa
dir=$1
ls $dir
exit 0
