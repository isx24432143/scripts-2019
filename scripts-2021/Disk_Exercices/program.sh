#! /bin/bash
# Solucions disk
# maig 2021
#-----------------
fileIN=$1
shift
for fsID in $*
do
   num=$(tr -s '[:blank:]' ':' < $fileIN | cut -d: -f5,6 | grep ":$fsID$" | wc -l)
   llista_mides=$(tr -s '[:blank:]' ':' < $fileIN | cut -d: -f5,6 | grep ":$fsID$" | cut -d: -f1)
   midaTotal=0
   for unaMida in $llista_mides
   do
     midaTotal=$((midaTotal+unaMida))
   done
   echo "$fsID: $num $midaTotal"
done
