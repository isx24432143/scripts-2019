#! /bin/bash


function loginboth(){
  file=/dev/stdin
  status=0
  if [ $# -eq 1 ]; then
    file=$1
  fi
  while read -r login
   do
     grep "^$login:" /etc/passwd &> /dev/null
     if [ $? -ne 0 ]; then
       echo "$login no valid"
       status=2
     else
       fsize $login
     fi
   done < $file					 								
   return $status						  
}


function loginfile(){
  status=0
  if [ $# -ne 1 ]; then
   echo "ERROR, Num Args no valid"
   return 1
  fi
  file=$1
  if [ -f $file ]; then
   while read -r login
   do
   fsize $login
   done < $file
  else
   echo "ERROR"
   status=2
  fi
  return $status

}


function loginargs(){
  status=0
  if [ $# -eq 0 ]; then
   echo "ERROR, Num Args no valid"
   return 1
  fi
  for login in $*:
  do
   grep "^$login:" /etc/passwd &> /dev/null
   if [ $? -ne 0 ]; then
    echo "$login no existeix"
    status=2
   else
    fsize $login
   fi
  done
  return $status
}


function fsize(){
  ERROR_NOLOGIN=2
  if [ $# -ne 1 ]; then
   echo "ERROR, Num Args no valid"
   return 1
  fi
  login=$1
  home=$(grep "^$login:" /etc/passwd | cut -d: -f6)
  if [ -z $home ]; then
   echo "$login no existeix" 
   return $ERROR_NOLOGIN
  else
    du -sh $home
    return 0
  fi
}


function exemple_filtra_GID(){
  MAX=500
  if [ $# -ne 1 ]; then
      echo "ERROR"
      return 1
  fi
  file=$1
  while read -r line
  do
    gid=$(echo $line | cut -d: -f4)
    if [ $gid -ge $MAX ]; then
       echo "$line"
    fi
  done < $file
}

function exemple_llista(){
  numlin=0
  file=/dev/stdin
  if [ $# -eq 1 ]; then
   file=$1
  fi
  while read -r line
  do
    echo "$numlin $line"
    ((numlin++))
  done < $file
}


