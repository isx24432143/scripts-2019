#! /bin/bash
# Exàmen administració d'usuaris
# Escola del Treball
# ASIX M01 ISO
# Abril 2021
# 14/04/2021
# Jamison Quelal León isx24432143
#----------------------------------



llista_shells=$(cut -d: -f7 /etc/passwd | sort | uniq)
for shell in $llista_shells:
do
   num=$(grep ":$shell$" /etc/passwd | wc -l)
   camps=$(grep ":$shell$" /etc/passwd | cut -d: -f1,3,4,6)
   echo "$shell($num)"
   echo -e "\t$camps" | sort -t: -k1,1 | sed -e 's/:/  /g'
done

