#! /bin/bash
KILL_PARTITION="/dev/mm1 /dev/mm2 /dev/mm3"
for partition in $KILL_PARTITION
do
   echo "dd if=/dev/zero of=$partition"
done
