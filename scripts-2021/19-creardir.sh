#! /bin/bash
# Jamison David Quelal León isx24432143 ASIX-M01-ISO
# Febrer 2021
# prog nomdir[...]
# Validar si existeix almenys un argument
# Fer que la ordre mkdir no generi cap mena sortida
# Fer nosaltres mateixos la sortida per >>/dev/stderr
#--------------------------------------------------
#Validar Argument
ERR_MKDIR=2
ERR_NARGS=1
status=0
if [ $# -lt 1 ]; then
  echo "ERROR: num args incorrecte"
  echo "USAGE: $0 nomdir[...]"
  exit $ERR_NARGS
fi

#Programa
for nom in $*
do
  mkdir $nom &> /dev/null
  if [ $? -ne $status ]; then
   echo "ERROR: $nom no creat" >> /dev/stderr
   status=$ERR_MKDIR
  else
   echo "Dir $nom creat"
  fi
done
exit $status
