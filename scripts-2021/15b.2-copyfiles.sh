#! /bin/bash
# Jamison David Quelal León isx24432143 ASIX-M01-ISO
# Febrer 2021
# Donat un arg: file i 2arg: dir, copia el file al directori
# $prog.sh file[...] dir-destí
#--------------------------------------
ERR_NARGS=1
ERR_NODIR=2
ERR_NOFILE=3
status=0

#Validar Argument
if [ $# -lt 2 ]; then
  echo "ERROR: num args incorrecte"
  echo "USAGE: $0 file[...] dir-destí"
  exit $ERR_NARGS
fi
ultim_arg=$(echo $* | sed 's/^.* //')
llistaFile=$(echo $* | sed 's/ [^ ]*$//')

#XIXA
echo "Nº Args: $#"
echo "Llista Args: $*"
echo "Ültim argument: $ultim_arg"
echo "Llista files: $llistaFile"

echo $* | cut -d' ' -f1-$(($#-1))
exit $status
